package kd_package;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Connector {

	Connection getConnection(){


        Connection con = null;
        

    
        String url = "jdbc:postgresql://localhost/kedar";
        String user = "kedar";
        String password = "exp01";

        try {
            con = DriverManager.getConnection(url, user, password);
            
        } catch (SQLException ex) {
            Logger lgr = Logger.getLogger(Version.class.getName());
            lgr.log(Level.SEVERE, ex.getMessage(), ex);

        } 
		return con;
    
	}
	
	
}
