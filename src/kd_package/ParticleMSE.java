/**
 * 
 */
package kd_package;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author kedar
 *
 */
public class ParticleMSE {
	
	private static int STEP_COUNT = 100;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub


		
		Connector connector = new Connector();

		Connection con = connector.getConnection();

		PreparedStatement get_each_timestamp=null,
				get_first_step=null,
				get_100_steps=null;

		ResultSet rs_start_timestamp=null,
				rs_first_step=null,
				rs_100_steps=null;

		String op_string;

		double[] sums = new double[STEP_COUNT];
		double[] values = null;
		
		double[] averages = new double[STEP_COUNT];
		double[] std_devs = new double[STEP_COUNT];

		List<double[]> exp_runs = new ArrayList<double[]>();

		for(int i=0;i<STEP_COUNT; i++){
			sums[i] = 0;
		}

		try {
			/*

			pst = con.prepareStatement("Select algo_name, start_timestamp, elapsed_time from exp_data ORDER BY start_timestamp, elapsed_time");


			ResultSet rs = pst.executeQuery();

			while(rs.next()){
				System.out.println(rs.getString("algo_name")+", "+rs.getLong(2)+", "+rs.getLong("elapsed_time"));
			}

			 */

			get_each_timestamp = con.prepareStatement(
					"SELECT DISTINCT start_timestamp" +
							" from exp_data" +
							" where algo_name = 'SNPF' AND" +
							" max_particles = 5000 AND" +
							" max_nested_particles = 10" +
					" ORDER BY start_timestamp");



			rs_start_timestamp = get_each_timestamp.executeQuery();

			int i=0, j=0;
			while(rs_start_timestamp.next()){ //First Loop

				i = 0;
				values = new double[STEP_COUNT];

				get_first_step = con.prepareStatement(
						"SELECT elapsed_time" +
								" FROM exp_data" +
								" WHERE start_timestamp="+rs_start_timestamp.getLong(1)+
								" AND other_robot_distance > 0.0" +
								" AND other_robot_distance < 1.0" +
								"limit(1)"
						);

				rs_first_step = get_first_step.executeQuery();

				if(rs_first_step.next()){
					//System.out.println(++i +") "+rs_start_timestamp.getLong("start_timestamp")+" "+rs_first_step.getLong("elapsed_time"));

					get_100_steps = con.prepareStatement(
							"SELECT particle_mse" +
									" FROM exp_data" +
									" WHERE start_timestamp="+rs_start_timestamp.getLong("start_timestamp")+
									" AND elapsed_time>"+ rs_first_step.getLong("elapsed_time") +
									"limit("+STEP_COUNT+")"
							);

					rs_100_steps = get_100_steps.executeQuery();


					op_string = "";
					while(rs_100_steps.next()){ //Second Loop
						if(i==0) {
							op_string = op_string + rs_start_timestamp.getLong("start_timestamp");
						}
						op_string = op_string + ", "+rs_100_steps.getDouble(1);
						sums[i] = sums[i]+rs_100_steps.getDouble(1);
						values[i] = rs_100_steps.getDouble(1);
						i++;
					}

					if(i==STEP_COUNT){
						exp_runs.add(values);
						System.out.println(op_string);
					}
					

					j++;


				}

			}//End First Loop
			
			
			Iterator<double[]> iter = exp_runs.iterator();
			double[] temp_values = new double[STEP_COUNT];
			double[] sum_squared_differences = new double[STEP_COUNT];
			
			int run_count = exp_runs.size();
			
			
			for(int k=0; k<STEP_COUNT; k++){
				sum_squared_differences[k] = 0;
				averages[k] = sums[k]/run_count;
				
			}
			
			
			while(iter.hasNext()){
				
				temp_values = iter.next();
				
				for(int step=0; step<STEP_COUNT;step++){
					
					sum_squared_differences[step] = sum_squared_differences[step] + (temp_values[step]-averages[step]) * (temp_values[step]-averages[step]);
					
				}
			}
			
			System.out.println("\n\n");

			
			for(int k=0; k<STEP_COUNT; k++){
				std_devs[k] = Math.sqrt(sum_squared_differences[k]/run_count);
				System.out.print(", "+averages[k]);
			}
			System.out.println("\n");
			for(int k=0; k<STEP_COUNT; k++){
				System.out.print(", "+std_devs[k]);
			}
			
			
			



			
			
			
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try {

				if(get_each_timestamp!=null) get_each_timestamp.close();
				if(get_first_step!=null) get_first_step.close();
				if(get_100_steps!=null) get_100_steps.close();

				if(rs_start_timestamp!=null) rs_start_timestamp.close();
				if(rs_first_step!=null) rs_first_step.close();
				if(rs_100_steps!=null) rs_100_steps.close();

				if (con != null) {
					con.close();
				}

			} catch (SQLException ex) {
				Logger lgr = Logger.getLogger(Version.class.getName());
				lgr.log(Level.WARNING, ex.getMessage(), ex);
			}
		}

	}













}
